import React, { useState, useEffect } from 'react';

export default function Dashboard() {
  const [tasks, setTasks] = useState([]);
  const [task, setTask] = useState('');
  const [students, setStudents] = useState([]);
  const [selectedStudent, setSelectedStudent] = useState('');
  
  useEffect(() => {
    // Fetch initial data for tasks and students
    fetchTasks();
    fetchStudents();
  }, []);

  const fetchTasks = async () => {
    // Fetch tasks from the backend
    const response = await fetch('/api/tasks');
    const data = await response.json();
    setTasks(data);
  };

  const fetchStudents = async () => {
    // Fetch students from the backend
    const response = await fetch('/api/students');
    const data = await response.json();
    setStudents(data);
  };

  const handleAssignTask = async () => {
    // Assign task to student
    const response = await fetch('/api/assign-task', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ task, student: selectedStudent })
    });
    if (response.ok) {
      fetchTasks();
    }
  };

  const handleAttemptTask = (taskId) => {
    // Redirect to the code editor
    window.location.href = '/code-editor?taskId=${taskId}';
  };

  return (
    <div>
      <h1>Dashboard</h1>
      
      <div className="task-form">
        <h2>Assign Task</h2>
        <input
          type="text"
          value={task}
          onChange={(e) => setTask(e.target.value)}
          placeholder="Task Description"
        />
        <select
          value={selectedStudent}
          onChange={(e) => setSelectedStudent(e.target.value)}
        >
          <option value="">Select Student</option>
          {students.map(student => (
            <option key={student.id} value={student.id}>{student.name}</option>
          ))}
        </select>
        <button onClick={handleAssignTask}>Assign Task</button>
      </div>
      
      <div className="task-list">
        <h2>Tasks</h2>
        <ul>
          {tasks.map(task => (
            <li key={task.id}>
              {task.description} - Assigned to: {task.studentName}
              <button onClick={() => handleAttemptTask(task.id)}>Attempt it</button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}